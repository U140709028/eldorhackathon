var MorrisChartsDemo= {
    init:function() {
        new Morris.Line( {
            element:"m_morris_1", data:[ {
                y: "Monday", a: 30, b: 20
            }
            , {
                y: "Tuesday", a: 20, b: 15
            }
            , {
                y: "Wednesday", a: 10, b: 5
            }
            , {
                y: "Thurday", a: 40, b: 30
            }
            , {
                y: "Sunday", a: 50, b: 40
            }
            , {
                y: "Saturday", a: 75, b: 65
            }
            , {
                y: "Sunday", a: 90, b: 85
            }
            ], xkey:"y", ykeys:["a", "b"], labels:["Values A", "Values B"]
        }
        ),
        new Morris.Area( {
            element:"m_morris_2", data:[ {
                y: "2006", a: 100, b: 90
            }
            , {
                y: "2007", a: 75, b: 65
            }
            , {
                y: "2008", a: 50, b: 48
            }
            , {
                y: "2009", a: 75, b: 0
            }
            , {
                y: "2010", a: 50, b: 40
            }
            , {
                y: "2011", a: 75, b: 65
            }
            , {
                y: "2012", a: 100, b: 90
            }
            ], xkey:"y", ykeys:["a", "b"], labels:["Açılan Sorular", "Cevaplanan Sorular"]
        }
        ),
        new Morris.Bar( {
            element:"m_morris_3", data:[ {
                y: "BMW", a: 100
            }
            , {
                y: "Mercedes", a: 75
            }
            , {
                y: "Porche", a: 50
            }
            , {
                y: "Fiat", a: 75
            }
            , {
                y: "Maserati", a: 50
            }
            , {
                y: "Ferrasi", a: 75
            }
            , {
                y: "Renault", a: 100,b:40
            }
            ], xkey:"y", ykeys:["a","b"], labels:["Coil Ignition","New Product"]
        }
        ),

        new Morris.Bar( {
            element:"m_morris_5", data:[ {
                y: "BMW", a: 100
            }
            , {
                y: "Mercedes", a: 75
            }
            , {
                y: "Porche", a: 50
            }
            , {
                y: "Fiat", a: 75
            }
            , {
                y: "Maserati", a: 50
            }
            , {
                y: "Ferrasi", a: 75
            }
            , {
                y: "Renault", a: 100,b:40
            }
            ], xkey:"y", ykeys:["a","b"], labels:["Coil Ignition","New Product"]
        }
        ),

        new Morris.Bar( {
            element:"m_morris_6", data:[ {
                y: "BMW", a: 100
            }
            , {
                y: "Mercedes", a: 75
            }
            , {
                y: "Porche", a: 50
            }
            , {
                y: "Fiat", a: 75
            }
            , {
                y: "Maserati", a: 50
            }
            , {
                y: "Ferrasi", a: 75
            }
            , {
                y: "Renault", a: 100,b:40
            }
            ], xkey:"y", ykeys:["a","b"], labels:["Coil Ignition","New Product"]
        }
        ),
        new Morris.Donut( {
            element:"m_morris_4", data:[ {
                label: "Download Sales", value: 12
            }
            , {
                label: "In-Store Sales", value: 30
            }
            , {
                label: "Mail-Order Sales", value: 20
            }
            ]
        }
        )
    }
}

;
jQuery(document).ready(function() {
    MorrisChartsDemo.init()
}

);
